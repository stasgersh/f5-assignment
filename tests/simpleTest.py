import allure


from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

# config_webdriver
#options = Options()
#options.add_argument('--headless')
#options.add_argument('--no-sandbox')
service = Service(executable_path=ChromeDriverManager().install())
driver = webdriver.Chrome(service=service)

#driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)


# Load a page
driver.get("https://the-internet.herokuapp.com/context_menu")

content = driver.find_element(By.ID, 'content').text


class TestPage:

    @allure.title("Home page - contains text")
    @allure.description(
        "Test should SUCCESS! Check if home page contains  - Right-click in the box below to see one called "
        "'the-internet'")
    def test_verify_page_contains_text(self):
        assert content.__contains__("Right-click in the box below to see one called 'the-internet'")

    @allure.title("Home page - not contains text")
    @allure.description("Test should FAIL! Check if home page not contains  - Alibaba")
    def test_verify_page_not_contains_text(self):
        assert content.__contains__("Alibaba"), "Alibaba was not found on Page!!!"


driver.quit()
