# f5-assignment

## Project pre-requisite
To install all packages that used in project, run:
- pip install -r requirements.txt

#### Generate Test Report
To generate all tests report using Allure you need to run tests by command first:
- To install allure, run: _"npm install -g allure-commandline --save-dev"_
- pytest <test.py> --alluredir=<reports directory path>
- allure generate [allure_output_dir] --clean
- allure serve <reports directory path>

### Allure report screenshots from tests run

![img.png](allure_landing.png)

![img_1.png](allure_tests.png)